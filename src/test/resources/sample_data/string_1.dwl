%dw 1.0
%output application/java
---
"org.mule.api.MessagingException: Json content is not compliant with schema
com.github.fge.jsonschema.core.report.ListProcessingReport: failure
--- BEGIN MESSAGES ---
error: string "FISH" is too long (length: 4, maximum allowed: 3)
    level: "error"
    schema: {"loadingURI":"file:/C:/AnypointStudio/workspace/.mule/apps/SandboxService/classes/schemas/requestSchema.json#","pointer":"/properties/base_currency_code"}
    instance: {"pointer":"/base_currency_code"}
    domain: "validation"
    keyword: "maxLength"
    value: "FISH"
    found: 4
    maxLength: 3
error: instance value ("BuyI") not found in enum (possible values: ["Buy","Sell"])
    level: "error"
    schema: {"loadingURI":"file:/C:/AnypointStudio/workspace/.mule/apps/SandboxService/classes/schemas/requestSchema.json#","pointer":"/properties/buy_or_sell"}
    instance: {"pointer":"/buy_or_sell"}
    domain: "validation"
    keyword: "enum"
    value: "BuyI"
    enum: ["Buy","Sell"]
---  END MESSAGES  ---
 (org.mule.module.json.validation.JsonSchemaValidationException)."