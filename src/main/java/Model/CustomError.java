package Model;

public class CustomError {
	String ErrorDescription;
	String Value;
	
	CustomError() {
		this.ErrorDescription = "test";
		this.Value = "test";
	}
	
	public void setErrorDescription(String ErrorDescription) {
		this.ErrorDescription = ErrorDescription;
	}
	
	public String getErrorDescription() {
		return this.ErrorDescription;
	}
	
	public void setValue(String Value) {
		this.Value = Value;
	}
	
	public String getValue() {
		return this.Value;
	}
}
